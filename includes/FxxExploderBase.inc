<?php

abstract class FxxExploderBase implements FxxExploder {

  protected $type = NULL;
  protected $exportVar = NULL;

  public function __construct($type, $options = array()) {
    $this->type = $type;

    if (!empty($options['export_var'])) {
      $this->exportVar = $options['export_var'];
    }

    if (!$this->exportVar) {
      throw new Exception(t('No exportVar property set on !class', array('!class' => __CLASS__)));
    }
  }

  public function explode($feature) {
    $info = features_get_features($feature);

    if (!$info->status) {
      return;
    }

    $files = array();

    if (empty($info->info['features'][$this->type])) {
      return $files;
    }

    foreach ($info->info['features'][$this->type] as $identifier) {
      $files[$this->fileName($feature, $identifier)] = "<?php\n\n" . $this->exportComponent($identifier);
    }

    return $files;
  }

  public function exportVar() {
    return $this->exportVar;
  }

  /**
   * @param $feature
   *   The name of the Feature module.
   * @param $identifier
   *   The exportable identifier.
   *
   * @return string
   *   The name of the file into which the export will be written.
   */
  protected function fileName($feature, $identifier) {
    $filename = "$feature.{$this->type}.$identifier.inc";
    $pattern = '/[^0-9a-z\._]/i';
    $filename = preg_replace($pattern, '__', $filename);
    return $filename;
  }

  /**
   * @param string $identifier
   *   The exportable identifier.
   *
   * @return string
   *   The code to go into the exploded export file.
   */
  abstract protected function exportComponent($identifier);

}
