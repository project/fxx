<?php

class FxxPageManagerExploder extends FxxExploderBase {

  protected $exportVar = 'pages';

  function exportComponent($identifier) {
    page_manager_get_task('page');

    $code = array();

    if ($page = page_manager_page_load($identifier)) {
      $code[] = page_manager_page_export($page, TRUE);
      $code[] = "\${$this->exportVar}['$identifier'] = \$page;\n";

    }

    return implode("\n", $code);
  }

}
