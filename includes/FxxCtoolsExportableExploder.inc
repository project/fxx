<?php

class FxxCtoolsExportableExploder extends FxxExploderBase {

  protected $exportVar = 'export';
  protected $componentVar = NULL;

  public function __construct($type, $options = array()) {
    parent::__construct($type, $options);

    ctools_include('export');
    $schema = ctools_export_get_schema($this->type);

    if (empty($schema['export'])) {
      throw new Exception(t('Component !type does not declare CTools Export schema information.', array('!type' => $this->type)));
    }

    if (empty($schema['export']['identifier'])) {
      throw new Exception(t('Component !type does not declare a custom CTools Export schema \'identifier\' value.', array('!type' => $this->type)));
    }

    $this->componentVar = $schema['export']['identifier'];
  }

  protected function exportComponent($identifier) {
    $code = '';

    if ($object = ctools_export_crud_load($this->type, $identifier)) {
      $code .= ctools_export_crud_export($this->type, $object, '');
      $code .= "\${$this->exportVar}[" . ctools_var_export($identifier) . "] = \${$this->componentVar};\n\n";
    }

    return $code;
  }

}
