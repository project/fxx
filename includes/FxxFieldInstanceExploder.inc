<?php

class FxxFieldInstanceExploder extends FxxExploderBase {

  protected $exportVar = 'field_instances';

  protected function exportComponent($identifier) {
    features_include();
    module_load_include('inc', 'features', 'features.export');

    $translatables = array();

    if ($instance = features_field_instance_load($identifier)) {
      _field_instance_features_export_sort($instance);
      $field_export = features_var_export($instance, '  ');
      $instance_prefix = '// Exported field_instance: ';
      $instance_identifier = features_var_export($identifier);
      if (features_field_export_needs_wrap($instance_prefix, $instance_identifier)) {
        $code[] = rtrim($instance_prefix);
        $code[] = "// {$instance_identifier}";
      }
      else {
        $code[] = $instance_prefix . $instance_identifier;
      }
      $code[] = "\$field_instances[{$instance_identifier}] = {$field_export};";
      $code[] = "";

      if (!empty($instance['label'])) {
        $translatables[] = $instance['label'];
      }
      if (!empty($instance['description'])) {
        $translatables[] = $instance['description'];
      }
    }

    if (!empty($translatables)) {
      $code[] = features_translatables_export($translatables, '  ');
    }

    $code = implode("\n", $code);

    return $code;
  }

}
