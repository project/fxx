<?php

interface FxxExploder {

  /**
   * Instantiate a new FxxExploder.
   *
   * @param string $type
   *   Component type.
   * @param array $options
   *   Optional data passed to the exploder.
   */
  public function __construct($type, $options = array());

  /**
   * Generate exploded file content.
   *
   * @param string $feature
   *   Name of the Feature module.
   * @return array
   *   An exploded export file contents, keyed by file name.
   */
  public function explode($feature);

  /**
   * The name of the variable in the code containing the exported components.
   *
   * @return string
   */
  public function exportVar();

}
