<?php

class FxxFieldBaseExploder extends FxxExploderBase {

  protected $exportVar = 'field_bases';

  protected function exportComponent($identifier) {
    features_include();
    module_load_include('inc', 'features', 'features.export');

    $code = array();

    if ($field = features_field_base_load($identifier)) {
      unset($field['columns']);
      unset($field['foreign keys']);
      // Only remove the 'storage' declaration if the field is using the default
      // storage type.
      if ($field['storage']['type'] == variable_get('field_storage_default', 'field_sql_storage')) {
        unset($field['storage']);
      }
      // If we still have a storage declaration here it means that a non-default
      // storage type was altered into to the field definition. And noone would
      // never need to change the 'details' key, so don't render it.
      if (isset($field['storage']['details'])) {
        unset($field['storage']['details']);
      }

      _field_instance_features_export_sort($field);
      $field_export = features_var_export($field, '  ');
      $field_prefix = '  // Exported field_base: ';
      $field_identifier = features_var_export($identifier);
      if (features_field_export_needs_wrap($field_prefix, $field_identifier)) {
        $code[] = rtrim($field_prefix);
        $code[] = "  // {$field_identifier}";
      }
      else {
        $code[] = $field_prefix . $field_identifier;
      }
      $code[] = "  \$field_bases[{$field_identifier}] = {$field_export};";
      $code[] = "";
    }

    $code = implode("\n", $code);

    return $code;
  }

}
