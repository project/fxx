<?php

class FxxViewsExploder extends FxxExploderBase {

  protected $exportVar = 'export';

  protected function exportComponent($identifier) {

    $code = array();

    if ($view = views_get_view($identifier)) {
      $code[] = views_export_view($view);
      $code[] = "\$export['$identifier'] = \$view;";
    }

    $code = implode("\n", $code);

    return $code;
  }

}
