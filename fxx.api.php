<?php

/**
 * @file
 * Features Export Explode API definitions.
 */

/**
 * Map FxxExploder classes for Features component types.
 *
 * Provide an array of information used to explode various Features components,
 * keyed by component type. Each element is either a string which is a valid
 * name of a class which implements FxxExploder, or an array which must include
 * a 'class' element along set to a valid name of a class implementing
 * FxxExploder with other parameters to pass along to the class.
 *
 * @see hook_fxx_info_alter()
 */
function hook_fxx_info() {
  return array(
    'component_type_a' => 'FxxExploderClassNameForA',
    'component_type_b' => array(
      'class' => 'FxxExploderClassNameForB',
      'option_1' => 'abc',
      'option_2' => 'def',
    ),
  );
}

/**
 * Alter the FxxExploder class mappings.
 *
 * @see hook_fxx_info()
 */
function hook_fxx_info_alter(&$info) {
  $info['component_type_a'] = 'CustomFxxExploderClassNameForA';
}
