<?php

/**
 * @file
 * Features Builder Drush integration.
 */

/**
 * Implements hook_drush_command().
 */
function fxx_drush_command() {
  $commands['features-explode'] = array(
    'description' => dt('Explode Features exported code.'),
    'aliases' => array('fxx'),
  );

  return $commands;
}

/**
 * Drush command to explode Features exports.
 */
function drush_fxx_features_explode() {
  if (!drush_confirm(dt('Are you sure you want to explode your Features?'))) {
    return drush_user_abort();
  }

  module_load_include('inc', 'fxx', 'fxx.explode');
  fxx_export_explode();
}
