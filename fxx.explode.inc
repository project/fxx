<?php

/**
 * @file
 * Features Export Explode explosion code.
 */

/**
 * Explode the Features.
 */
function fxx_export_explode() {
  $info = fxx_info();
  $features = features_get_features();

  foreach ($features as $feature) {
    $types = array_intersect(array_keys($info), $feature->components);
    foreach ($types as $type) {
      $exploder = fxx_get_exploder($type);
      $files = $exploder->explode($feature->name);
      if ($files) {
        fxx_write_exploded_files($feature->name, $files);
        fxx_rewrite_default_hook($feature->name, $type, $files);
      }
    }
  }
}

/**
 * Find the "default" include file containing the component code.
 *
 * @param string $feature
 *   Feature module name.
 * @param string $type
 *   Component type.
 *
 * @return string
 *   The file name of the file, NULL if unable to determine.
 */
function fxx_get_default_file($feature, $type) {
  $info = features_get_components($type);

  switch ($info['default_file']) {
    case FEATURES_DEFAULTS_INCLUDED:
      $file = "$feature.features.$type";
      break;
    case FEATURES_DEFAULTS_CUSTOM:
      if (!empty($info['default_filename'])) {
        $file = "$feature." . $info['default_filename'];
      } else {
        $file = FALSE;
      }
      break;
    default:
      $file = "$feature.features";
  }

  if ($file) {
    return $file;
  }
}

/**
 * Find the "default" hook function name.
 *
 * @param string $feature
 *   Feature module name.
 * @param string $type
 *   Component type.
 *
 * @return string
 *   The file name of the file, NULL if unable to determine.
 */
function fxx_get_default_hook($feature, $type) {
  $info = features_get_components($type);
  return $feature . '_' . $info['default_hook'];
}

/**
 * Rewrites the default hook which contained the export.
 *
 * @param string $feature
 *   Feature module name.
 * @param string $type
 *   Component type.
 * @param array $files
 *   Names of the exploded export files.
 */
function fxx_rewrite_default_hook($feature, $type, $files) {
  $path = drupal_get_path('module', $feature);

  $exploder = fxx_get_exploder($type);
  $export_var = '$' . $exploder->exportVar();

  $file = fxx_get_default_file($feature, $type) . '.inc';
  $default_hook = fxx_get_default_hook($feature, $type);

  $contents = file($path . '/' . $file);
  $rewrite = array();

  $pattern = '/function ' . $default_hook . '\(/';
  $state = 0;

  // Iterate over the default export file, find the default function and replace
  // it's contents with includes to the exploded files.
  foreach ($contents as $line) {
    if (substr($line, -1, 1) == "\n") {
      $line = substr($line, 0, strlen($line) - 1);
    }
    switch ($state) {
      case 0:
        // Haven't yet found start of function.
        preg_match($pattern, $line, $matches);
        if ($matches) {
          $state = 1;
        }
        $rewrite[] = $line;
        break;
      case 1:
        // In the function.
        if ($line == '}') {
          $rewrite[] = "  {$export_var} = array();";
          foreach (array_keys($files) as $include) {
            $rewrite[] = "  include __DIR__ . '/{$include}';";
          }
          $rewrite[] = "  return {$export_var};";
          $rewrite[] = $line;
          $state = 2;
        }
        break;
      default:
        // Past the function.
        $rewrite[] = $line;
        break;
    }
  }

  // End with empty line.
  $rewrite[] = '';

  file_unmanaged_save_data(implode("\n", $rewrite), $path . '/' . $file, FILE_EXISTS_REPLACE);
}

/**
 * Write the exploded export files.
 *
 * @param string $feature
 *   Feature module name.
 * @param array $files
 *   Exploded export files, keyed by file name.
 */
function fxx_write_exploded_files($feature, $files) {
  $path = drupal_get_path('module', $feature);
  foreach ($files as $filename => $code) {
    file_unmanaged_save_data($code, "$path/$filename", FILE_EXISTS_REPLACE);
  }
}

/**
 * Gather FXX integration from enabled modules.
 *
 * @param string $type
 *   Component type, optional.
 *
 * @return array
 *   FXX info array for the specified type. All types if no type specified.
 */
function fxx_info($type = NULL) {
  $info = drupal_static(__FUNCTION__, NULL);

  if ($info === NULL) {
    $info = module_invoke_all('fxx_info');
    drupal_alter(__FUNCTION__, $identifiers);
  }

  if ($type) {
    return !empty($info[$type]) ? $info[$type] : NULL;
  }

  return $info;
}

/**
 * Get the exploder class for a component type.
 *
 * @param string $type
 *   Component type.
 *
 * @return FxxExploder
 *   An instantiated FxxExploder object.
 *
 * @throws Exception
 */
function fxx_get_exploder($type) {
  $exploders = drupal_static('fxx_exploders', array());

  if (empty($exploders[$type])) {
    $info = fxx_info($type);

    if (empty($info)) {
      throw new Exception(t('Exploder class not declared for !type.', array('!type' => $type)));
    }

    if (is_array($info)) {
      $class = $info['class'];
      $exploders[$type] = new $class($type, $info);
    } else {
      $exploders[$type] = new $info($type);
    }
  }

  return $exploders[$type];
}
